# Gilded Rose Refactoring Kata

## Requirements:
* Java 8
* Maven

## How to run
```
mvn clean test
```

# Own comments
To calculate the new "sellIn" field, the logic is still in the ItemWrapper. It seems to me that this could be split up, 
using the same way as the QualityCalculationStrategy. However, this seems overkill for now as there are would only be two implementations. It feels too much like overengineering to me at this point, which is why I did not do this.

I opted fairly early to create an "ItemWrapper", as I felt the main logic should be inside the Item class, but this class is off limits. The ItemWrapper is a way to work around that.
Afterwards I added an ItemService. This allowed me to make the QualityCalculationStrategyFactory not static anymore. 
As a result, this one should be easier to test in combination with a DI framework: use DI to inject different strategies, but for testing just use some dummy strategies.  

Introducing the ItemService also makes it easier to add a strategy to calculate the sellIn field, if the need arises too.

As a side effect, there is barely any logic left in the ItemWrapper. However, it contains some useful helper functions, such as "isBackstagePass()", which helps reduce some double code.
However, admittedly, this class is not as useful as it was in the beginning. Without the helper functions that are used here and there, I would opt to remove it.

