package com.gildedrose;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GildedRoseTest {

    @Test
    public void updateQuality_NameDoesNotChange() {
        Item[] items = new Item[]{new Item("foo", 0, 0)};
        GildedRose app = new GildedRose(items);

        app.updateQuality();

        assertThat(app.items[0].name).isEqualTo("foo");
    }

    @Test
    public void updateQuality_SellInDateAlwaysDrops() {
        Item item = new Item("anItem", 15, 15);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.sellIn).isEqualTo(14);
    }

    @Test
    public void regularItem_QualityIsUpdated_QualityDrops() {
        Item item = new Item("foo", 2, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(19);
    }

    @Test
    public void regularItemWithPassedSellDate_QualityIsUpdated_QualityDropsDouble() {
        Item item = new Item("foo", 0, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(18);
    }

    @Test
    public void qualityCanNeverDropBelowZero() {
        Item item = new Item("foo", 0, 0);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(0);
    }

    @Test
    public void AgedBrie_QualityIsUpdated_QualityIncreases() {
        Item item = new Item(GildedRose.AGED_BRIE, 2, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(21);
    }

    @Test
    public void AgedBriePastSellDate_QualityIsUpdated_QualityIncreasesDouble() {
        Item item = new Item(GildedRose.AGED_BRIE, 0, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(22);
    }

    @Test
    public void qualityCanNeverGoAboveFifty() {
        Item item = new Item(GildedRose.AGED_BRIE, 0, 50);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(50);
    }

    @Test
    public void backstagePasses_MoreThan10DaysLeft_IncreasesInValue() {
        Item item = new Item(GildedRose.BACKSTAGE_PASSES, 15, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(21);
    }

    @Test
    public void backstagePasses_10DaysLeft_IncreasesValueAtDoubleRate() {
        Item item = new Item(GildedRose.BACKSTAGE_PASSES, 10, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(22);
    }

    @Test
    public void backstagePasses_6DaysLeft_IncreasesValueAtDoubleRate() {
        Item item = new Item(GildedRose.BACKSTAGE_PASSES, 6, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(22);
    }

    @Test
    public void backstagePasses_5DaysLeft_IncreasesValueAtTripleRate() {
        Item item = new Item(GildedRose.BACKSTAGE_PASSES, 5, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(23);
    }

    @Test
    public void backstagePasses_NoDaysLeft_ValueDropsToZero() {
        Item item = new Item(GildedRose.BACKSTAGE_PASSES, 0, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(0);
    }

    @Test
    public void conjured_Item_DecreasesInValueAtDoubleRate() {
        Item item = new Item("Conjured cake", 5, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(18);
    }

    @Test
    public void sulfurasAlwaysHasQuality80() {
        Item item = new Item(GildedRose.SULFURAS, 15, 20);
        GildedRose gildedRose = new GildedRose(item);

        gildedRose.updateQuality();

        assertThat(item.quality).isEqualTo(80);
        assertThat(item.sellIn).isEqualTo(15);
    }
}
