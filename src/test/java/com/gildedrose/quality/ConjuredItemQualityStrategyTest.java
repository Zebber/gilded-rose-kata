package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class ConjuredItemQualityStrategyTest {

    private final ConjuredItemQualityStrategy strategy = new ConjuredItemQualityStrategy();

    @Test
    public void canCalculateFor_Conjured_ReturnsTrue() {
        ItemWrapper itemWrapper = Mockito.mock(ItemWrapper.class);
        when(itemWrapper.getName()).thenReturn("Conjured platypus");

        boolean result = strategy.canCalculateFor(itemWrapper);

        assertThat(result).isTrue();
    }

    @Test
    public void canCalculateFor_SomethingElse_ReturnsFalse() {
        ItemWrapper itemWrapper = Mockito.mock(ItemWrapper.class);
        when(itemWrapper.getName()).thenReturn("Something else");

        boolean result = strategy.canCalculateFor(itemWrapper);

        assertThat(result).isFalse();
    }

    @Test
    public void calculate() {
        int result = strategy.calculateNewQualityWithoutLimits(20, 5);

        assertThat(result).isEqualTo(18);
    }

    @Test
    public void calculate_AfterSaleDate_DegradesTwiceAsFast() {
        int result = strategy.calculateNewQualityWithoutLimits(20, 0);

        assertThat(result).isEqualTo(16);
    }
}