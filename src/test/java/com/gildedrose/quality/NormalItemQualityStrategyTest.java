package com.gildedrose.quality;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NormalItemQualityStrategyTest {

    private final NormalItemQualityStrategy strategy = new NormalItemQualityStrategy();

    @Test
    public void calculateNewQuality() {
        int result = strategy.calculateNewQualityWithinLimits(50, 2);

        assertThat(result).isEqualTo(49);
    }

    @Test
    public void calculateNewQuality_CannotDropBelowZero() {
        int result = strategy.calculateNewQualityWithinLimits(0, 2);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void calculateNewQuality_PastSellDate_DecreasesAtDoubleRate() {
        int result = strategy.calculateNewQualityWithinLimits(50, 0);

        assertThat(result).isEqualTo(48);
    }
}