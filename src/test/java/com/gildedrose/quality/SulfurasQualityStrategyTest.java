package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class SulfurasQualityStrategyTest {

    private final SulfurasQualityStrategy strategy = new SulfurasQualityStrategy();

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private ItemWrapper itemWrapper;

    @Test
    public void canCalculateFor_Sulfuras_ReturnsTrue() {
        when(itemWrapper.isSulfuras()).thenReturn(true);

        assertThat(strategy.canCalculateFor(itemWrapper)).isTrue();
    }

    @Test
    public void canCalculateFor_AnythingElse_ReturnsFalse() {
        when(itemWrapper.isSulfuras()).thenReturn(false);

        assertThat(strategy.canCalculateFor(itemWrapper)).isFalse();
    }
}