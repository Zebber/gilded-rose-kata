package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class AgedBrieItemQualityStrategyTest {

    private final AgedBrieItemQualityStrategy strategy = new AgedBrieItemQualityStrategy();

    @Test
    public void calculateNewQuality_QualityIncreases() {
        int result = strategy.calculateNewQualityWithinLimits(20, 2);

        assertThat(result).isEqualTo(21);
    }

    @Test
    public void calculateNewQuality_PastSellDate_QualityIncreasesDouble() {
        int result = strategy.calculateNewQualityWithinLimits(20, 0);

        assertThat(result).isEqualTo(22);
    }

    @Test
    public void calculateNewQuality_MaxQualityIs50() {
        int result = strategy.calculateNewQualityWithinLimits(50, 0);

        assertThat(result).isEqualTo(50);
    }

    @Test
    public void canCalculateFor_AgedBrie_ReturnsTrue() {
        ItemWrapper itemWrapper = Mockito.mock(ItemWrapper.class);
        when(itemWrapper.isAgedBrie()).thenReturn(true);

        assertThat(strategy.canCalculateFor(itemWrapper)).isTrue();
    }

    @Test
    public void canCalculateFor_AnythingElse_ReturnsFalse() {
        ItemWrapper itemWrapper = Mockito.mock(ItemWrapper.class);
        when(itemWrapper.isAgedBrie()).thenReturn(false);

        assertThat(strategy.canCalculateFor(itemWrapper)).isFalse();
    }
}