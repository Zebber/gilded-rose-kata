package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import org.junit.Test;
import org.mockito.Mockito;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class BackstagePassItemQualityStrategyTest {

    private final BackstagePassItemQualityStrategy strategy = new BackstagePassItemQualityStrategy();

    @Test
    public void backstagePasses_MoreThan10DaysLeft_IncreasesInValue() {
        int result = strategy.calculateNewQualityWithinLimits(20, 15);

        assertThat(result).isEqualTo(21);
    }

    @Test
    public void backstagePasses_10DaysLeft_IncreasesValueAtDoubleRate() {
        int result = strategy.calculateNewQualityWithinLimits(20, 10);

        assertThat(result).isEqualTo(22);
    }

    @Test
    public void backstagePasses_6DaysLeft_IncreasesValueAtDoubleRate() {
        int result = strategy.calculateNewQualityWithinLimits(20, 6);

        assertThat(result).isEqualTo(22);
    }

    @Test
    public void backstagePasses_5DaysLeft_IncreasesValueAtTripleRate() {
        int result = strategy.calculateNewQualityWithinLimits(20, 5);

        assertThat(result).isEqualTo(23);
    }

    @Test
    public void backstagePasses_NoDaysLeft_ValueDropsToZero() {
        int result = strategy.calculateNewQualityWithinLimits(20, 0);

        assertThat(result).isEqualTo(0);
    }

    @Test
    public void canCalculateFor_BackstagePass_ReturnsTrue() {
        ItemWrapper itemWrapper = Mockito.mock(ItemWrapper.class);
        when(itemWrapper.isBackstagePass()).thenReturn(true);

        assertThat(strategy.canCalculateFor(itemWrapper)).isTrue();
    }

    @Test
    public void canCalculateFor_AnythingElse_ReturnsFalse() {
        ItemWrapper itemWrapper = Mockito.mock(ItemWrapper.class);
        when(itemWrapper.isBackstagePass()).thenReturn(false);

        assertThat(strategy.canCalculateFor(itemWrapper)).isFalse();
    }
}