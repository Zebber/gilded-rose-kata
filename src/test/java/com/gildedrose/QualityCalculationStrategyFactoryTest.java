package com.gildedrose;

import com.gildedrose.quality.*;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class QualityCalculationStrategyFactoryTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private final QualityCalculationStrategyFactory factory = new QualityCalculationStrategyFactory();

    @Mock
    private ItemWrapper itemWrapper;


    @Test
    public void createStrategy() {
        QualityCalculationStrategy strategy = factory.createStrategy(itemWrapper);

        assertThat(strategy).isInstanceOf(NormalItemQualityStrategy.class);
    }

    @Test
    public void createStrategy_BackstagePasses() {
        when(itemWrapper.isBackstagePass()).thenReturn(true);

        QualityCalculationStrategy strategy = factory.createStrategy(itemWrapper);

        assertThat(strategy).isInstanceOf(BackstagePassItemQualityStrategy.class);
    }

    @Test
    public void createStrategy_AgedBrie() {
        when(itemWrapper.isAgedBrie()).thenReturn(true);

        QualityCalculationStrategy strategy = factory.createStrategy(itemWrapper);

        assertThat(strategy).isInstanceOf(AgedBrieItemQualityStrategy.class);
    }

    @Test
    public void createStrategy_Sulfuras() {
        when(itemWrapper.isSulfuras()).thenReturn(true);

        QualityCalculationStrategy strategy = factory.createStrategy(itemWrapper);

        assertThat(strategy).isInstanceOf(SulfurasQualityStrategy.class);
    }

    @Test
    public void createStrategy_Conjured() {
        when(itemWrapper.getName()).thenReturn("Conjured eggs");

        QualityCalculationStrategy strategy = factory.createStrategy(itemWrapper);

        assertThat(strategy).isInstanceOf(ConjuredItemQualityStrategy.class);
    }
}