package com.gildedrose;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

public class ItemServiceTest {

    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private ItemService itemService;

    @Mock
    private QualityCalculationStrategyFactory factory;
    @Mock
    private QualityCalculationStrategy strategy;

    private ItemWrapper itemWrapper = new ItemWrapper(new Item("aName", 50, 10));

    @Test
    public void updateItem() {
        when(factory.createStrategy(itemWrapper)).thenReturn(strategy);
        when(strategy.calculateNewQualityWithinLimits(10, 50)).thenReturn(20);

        itemService.updateItem(itemWrapper);

        assertThat(itemWrapper.getQuality()).isEqualTo(20);
        assertThat(itemWrapper.getSellIn()).isEqualTo(49);
    }
}