package com.gildedrose;

import java.util.stream.Stream;

class GildedRose {
    public static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    public static final String AGED_BRIE = "Aged Brie";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";

    Item[] items;

    private final ItemService itemService = ItemService.instance();

    public GildedRose(Item...items) {
        this.items = items;
    }

    public void updateQuality() {
        Stream.of(items)
                .map(ItemWrapper::new)
                .forEach(itemService::updateItem);
    }
}