package com.gildedrose;

public class ItemService {

    private QualityCalculationStrategyFactory factory;

    public ItemService(QualityCalculationStrategyFactory factory) {
        this.factory = factory;
    }

    public static ItemService instance() {
        return new ItemService(new QualityCalculationStrategyFactory());
    }

    public void updateItem(ItemWrapper item) {
        QualityCalculationStrategy strategy = factory.createStrategy(item);

        item.updateQuality(strategy.calculateNewQualityWithinLimits(item.getQuality(), item.getSellIn()));
        item.updateSellIn();
    }

}
