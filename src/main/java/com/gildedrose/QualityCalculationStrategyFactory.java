package com.gildedrose;

import com.gildedrose.quality.*;

import java.util.Arrays;
import java.util.List;

public class QualityCalculationStrategyFactory {

    private final List<QualityCalculationStrategy> strategies = Arrays.asList(
            new SulfurasQualityStrategy(),
            new AgedBrieItemQualityStrategy(),
            new BackstagePassItemQualityStrategy(),
            new ConjuredItemQualityStrategy()
    );

    public QualityCalculationStrategy createStrategy(ItemWrapper item) {
        return strategies
                .stream()
                .filter(strategy -> strategy.canCalculateFor(item))
                .findFirst()
                .orElse(new NormalItemQualityStrategy());
    }

}
