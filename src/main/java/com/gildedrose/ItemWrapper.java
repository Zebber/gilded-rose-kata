package com.gildedrose;

import static com.gildedrose.GildedRose.*;

public class ItemWrapper {

    private final Item item;

    public ItemWrapper(Item item) {
        this.item = item;
    }

    void updateQuality(int newQuality) {
        item.quality = newQuality;
    }

    void updateSellIn() {
        if (!isSulfuras()) {
            item.sellIn = item.sellIn - 1;
        }
    }

    public boolean isBackstagePass() {
        return item.name.equals(BACKSTAGE_PASSES);
    }

    public boolean isAgedBrie() {
        return item.name.equals(AGED_BRIE);
    }

    public boolean isSulfuras() {
        return item.name.equals(SULFURAS);
    }

    public String getName() {
        return item.name;
    }

    public int getQuality() {
        return item.quality;
    }

    public int getSellIn() {
        return item.sellIn;
    }
}
