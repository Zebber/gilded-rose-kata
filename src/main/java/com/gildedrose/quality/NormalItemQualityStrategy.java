package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import com.gildedrose.QualityCalculationStrategy;

public class NormalItemQualityStrategy extends QualityCalculationStrategy {

    @Override
    public int calculateNewQualityWithoutLimits(int quality, int sellIn) {
        return quality - sellByFactor(sellIn);
    }

    @Override
    public boolean canCalculateFor(ItemWrapper item) {
        return true;
    }

    private int sellByFactor(int sellIn) {
        if (sellIn <= 0) {
            return 2;
        }
        return 1;
    }
}
