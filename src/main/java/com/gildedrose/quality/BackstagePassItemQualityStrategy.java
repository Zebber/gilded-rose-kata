package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import com.gildedrose.QualityCalculationStrategy;

public class BackstagePassItemQualityStrategy extends QualityCalculationStrategy {

    @Override
    public int calculateNewQualityWithoutLimits(int quality, int sellIn) {
        if (pastSaleDate(sellIn)) {
            return 0;
        }

        quality++;

        if (atMostTenDaysLeft(sellIn)) {
            quality++;
        }

        if (atMostFiveDaysLeft(sellIn)) {
            quality++;
        }

        return quality;
    }

    @Override
    public boolean canCalculateFor(ItemWrapper item) {
        return item.isBackstagePass();
    }

    private boolean atMostFiveDaysLeft(int sellIn) {
        return sellIn < 6;
    }

    private boolean atMostTenDaysLeft(int sellIn) {
        return sellIn < 11;
    }

    private boolean pastSaleDate(int sellIn) {
        return sellIn <= 0;
    }
}
