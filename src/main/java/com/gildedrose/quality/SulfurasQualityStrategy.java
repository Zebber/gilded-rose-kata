package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import com.gildedrose.QualityCalculationStrategy;

public class SulfurasQualityStrategy extends QualityCalculationStrategy {
    @Override
    public int calculateNewQualityWithoutLimits(int quality, int sellIn) {
        return 80;
    }

    @Override
    public int calculateNewQualityWithinLimits(int quality, int sellIn) {
        return calculateNewQualityWithoutLimits(quality, sellIn);
    }

    @Override
    public boolean canCalculateFor(ItemWrapper item) {
        return item.isSulfuras();
    }
}
