package com.gildedrose.quality;

import com.gildedrose.ItemWrapper;
import com.gildedrose.QualityCalculationStrategy;

public class ConjuredItemQualityStrategy extends QualityCalculationStrategy {
    @Override
    public boolean canCalculateFor(ItemWrapper item) {
        if (item.getName() == null) {
            return false;
        }

        return item.getName().startsWith("Conjured");
    }

    @Override
    protected int calculateNewQualityWithoutLimits(int quality, int sellIn) {
        return quality - sellInFactor(sellIn);
    }

    private int sellInFactor(int sellIn) {
        if (sellIn <= 0) {
            return 4;
        }
        return 2;
    }
}
