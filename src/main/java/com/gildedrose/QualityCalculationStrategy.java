package com.gildedrose;

public abstract class QualityCalculationStrategy {

    private static final int MINIMUM_QUALITY = 0;
    private static final int MAX_QUALITY = 50;

    public abstract boolean canCalculateFor(ItemWrapper item);

    public int calculateNewQualityWithinLimits(int quality, int sellIn) {
        int newQuality = calculateNewQualityWithoutLimits(quality, sellIn);

        if (newQuality > MAX_QUALITY) {
            return MAX_QUALITY;
        }
        return Math.max(newQuality, MINIMUM_QUALITY);
    }

    protected abstract int calculateNewQualityWithoutLimits(int quality, int sellIn);


}
